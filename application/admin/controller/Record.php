<?php
// +----------------------------------------------------------------------
// | Description: 跟进记录
// +----------------------------------------------------------------------
// | Author:  Michael_xu | gengxiaoxu@5kcrm.com
// +----------------------------------------------------------------------

namespace app\admin\controller;

use think\Hook;
use think\Request;
use think\Db;
use PHPExcel_IOFactory;
use PHPExcel_Cell;
use PHPExcel;

class Record extends ApiCommon
{
    /**
     * 用于判断权限
     * @permission 无限制
     * @allow 登录用户可访问
     * @other 其他根据系统设置
    **/    
    public function _initialize()
    {
        $action = [
            'permission'=>[''],
            'allow'=>['index','save','update','delete']            
        ];
        Hook::listen('check_auth',$action);
        $request = Request::instance();
        $a = strtolower($request->action());        
        if (!in_array($a, $action['permission'])) {
            parent::_initialize();
        }
    }

    /**
     * 跟进记录列表
     * @return
     */
    public function index()
    {
        $param = $this->param;
        $by = $param['by'] ? : '';
        unset($param['by']);
        $recordModel = model('Record');
        $data = $recordModel->getDataList($param, $by);
        if (!$data) {
            return resultArray(['error' => $recordModel->getError()]);
        }
        return resultArray(['data' => $data]);
    }   

    /**
     * 客户导出
     * @author Michael_xu
     * @param 
     * @return
     */
    public function excelExport()
    {
        $param = $this->param;
        $userInfo = $this->userInfo;
        $param['user_id'] = $userInfo['id'];
        if ($param['customer_id']) {
           $param['customer_id'] = ['condition' => 'in','value' => $param['customer_id'],'form_type' => 'text','name' => ''];
           $param['is_excel'] = 1;
        }
        
        $excelModel = new \app\admin\model\Excel();
        // 导出的字段列表
        // $fieldModel = new \app\admin\model\Field();
        // $field_list = $fieldModel->getIndexFieldList('crm_customer', $userInfo['id']);
        $field_list = ['types', 'types_id', '跟进类型', '工作内容', '发报价', '邮寄样品', '客户问题', '客户需求', '工作计划'];
        
        // 文件名
        $file_name = '5kcrm_customer_record_'.date('Ymd');
        $param['pageType'] = 'all'; 
        $excelModel->exportCsv($file_name, $field_list, function($list) use ($param){
            $list = model('Record')->getDataList($param);
            return $list;
        });
    }

    /**
     * 跟进记录创建
     * @param
     * @return
     */
    public function save()
    {
        $recordModel = model('Record');
        $param = $this->param;

        if (isset($param['types_id']) && $param['types_id']) {
            $data = db('crm_customer')->where(['customer_id' => $param['types_id']])->update(['last_record_time' => time()]);
        }
        $userInfo = $this->userInfo;
        $param['create_user_id'] = $userInfo['id'];
        $resData = $recordModel->createData($param);
        if (!$resData) {
            return resultArray(['error' => $recordModel->getError()]);
        }
        //同时创建日程
        if ($param['is_event']) {
            $eventModel = new \app\oa\model\Event();
            $data['title'] = trim($param['content']);
            $data['content'] = trim($param['content']);
            $data['start_time'] = $param['next_time'] ? : time();
            $data['end_time'] = $param['next_time']+86399;
            $data['create_user_id'] = $userInfo['id'];
            if ($param['types'] == 'crm_customer') $data['customer_ids'] = $param['types_id'];
            $data['business_ids'] = $param['business_ids'];
            $data['contacts_ids'] = $param['contacts_ids'];            
            $resEvent = $eventModel->createData($data);          
        }
        return resultArray(['data' => '添加成功']);
    }

    /**
     * 跟进记录编辑
     * @param 
     * @return
     */
    public function update()
    {
        $recordModel = model('Record');
        $param = $this->param;
        $data = $recordModel->updateDataById($param, $param['id']);
        if (!$data) {
            return resultArray(['error' => $recordModel->getError()]);
        } 
        return resultArray(['data' => '编辑成功']);        
    }

    /**
     * 跟进记录删除
     * @param
     * @return
     */
    public function delete()
    {
        $recordModel = model('Record');
        $param = $this->param;
        $userInfo = $this->userInfo;
        //权限判断
        $dataInfo = $recordModel->getDataById($param['id']);
        if (!$dataInfo) {
            return resultArray(['error' => '数据不存在或已删除']);
        }
        //自己(24小时)或者管理员
        $adminTypes = adminGroupTypes($userInfo['id']);
        if (!in_array(1,$adminTypes) && ($dataInfo['create_user_id'] !== $userInfo['id'] && ((time()-$dataInfo['create_time']) > 86400))) {
            return resultArray(['error' => '超过24小时，不能删除']);
        }
        $resData = $recordModel->delDataById($param['id']);
        if (!$resData) {
            return resultArray(['error' => $recordModel->getError()]);
        }
        return resultArray(['data' => '删除成功']);
    }   
}
